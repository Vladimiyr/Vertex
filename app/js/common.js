$(document).ready(function () {
	"use strict";
    /*Menu*/
    $(function(){
        $('.nav .m-menu-button').click(function(){
            $(this).toggleClass("close-mobile-button");
            $('.main-nav').fadeToggle(100);
        });
    });

	 var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,
        height: 500,
        loop: true
    });

	var swiper = new Swiper('.swiper-container-as-services', {
        slidesPerView: 5,
         nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 20,
         loop: true,
        breakpoints: {
			600: {
				slidesPerView: 1,
			},
            750: {
                slidesPerView: 2,
            },
			1000: {
				slidesPerView: 3,
			},
            1200: {
                slidesPerView: 4,
            }
		}
    });

    var swiper = new Swiper('.swiper-container-as-sertificat', {
        slidesPerView: 4,
         nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 20,
         loop: true,
        breakpoints: {
            600: {
                slidesPerView: 1,
            },
            750: {
                slidesPerView: 2,
            },
            1000: {
                slidesPerView: 3,
            }
        }
    });

    // var swiper = new Swiper('.swiper-container-slog-services', {
    //     slidesPerView: 3,
    //      nextButton: '.swiper-button-next',
    //     prevButton: '.swiper-button-prev',
    //     paginationClickable: true,
    //     spaceBetween: 20,
    //     centeredSlides: true,
    //      loop: true,
    //     breakpoints: {
    //         600: {
    //             slidesPerView: 1,
    //         },
    //         900: {
    //             slidesPerView: 3,
    //         }
    //     }
    // });
    
    

    $('#inputLbl, input').click(function() {
        $(this).find('#inputLbl').removeClass('top');
    });

    $('#input').focusout(function() {
        if ($(this).val().trim() == '') {
            $('#inputLbl').addClass('top');
        }
    });

var head, //".accordion > h3"
    content, //".accordion > div"
    animateTime = 500;

//при клике по заголовку...
$(".accordion > h3").click(function () {

    head = $(this);
    content = $(this).next(".block-container");

    //Перебрать элементы массива (все h3)
    $(".accordion > h3").each(function (index) {
        let elem = $(this);

        //Проверить, есть ли уже растрытые блоки. Если есть - закрыть.
        if (elem.attr("class") === "opened") {
            elem.removeClass("opened");
            elem.next(".block-container").stop().removeClass("height").animate({ height: '0'}, animateTime);
        }
    });
    $(".accordion > h3").click(function () {
        head.next(".block-container").addClass("height");
      });
    //Добавить заголовку раскрываемого блока класс "opened"
    head.addClass("opened");
    // content.addClass("height");
     head.next(".block-container").css("height", "100%")

    //Плавно раскрыть блок с контентом
    autoHeightAnimate(content, animateTime);

    //Прокрутить страницу до раскрытого заголовка
    //это нужно, чтобы контент не уплывал вверх, особенно на маленьких экранах и при большом объеме контента
    setTimeout(function () {
     $("html, body").animate({
         scrollTop: head.offset().top
     }, animateTime);
    }, animateTime);

    /*****/
    /* Функция для - animate height: auto */
    function autoHeightAnimate(element, time) {
        var  // Получить высоту по умолчанию
        autoHeight = element.css('height', '100%').height(); // Сделать высоту auto

        // element.height(curHeight); //Переустановить высоту
        element.stop().animate({
            height: autoHeight
        }, time);
    }
});
        
$( "#heatingInput" ).blur(function() {
   var someI = $('#heatingInput').val();

   console.log($('#heatingInput').val());
   if(someI > 0){
       $('#heatingInput0').removeClass('hide');
       $('#heatingInput2, #heatingInput1').addClass('hide');
   }
   if(someI > 1){
       $('#heatingInput1').removeClass('hide');
       $('#heatingInput2').addClass('hide');
   }
   if(someI > 2){
       $('#heatingInput2').removeClass('hide');
   }
   if(someI == 0){
       $('#heatingInput0, #heatingInput1, #heatingInput2').addClass('hide');
   }
   if(someI == null){
       $('#heatingInput0, #heatingInput1, #heatingInput2').addClass('hide');
   }
});



$( "#underfloor-heatingInput" ).blur(function() {
   var someI = $('#underfloor-heatingInput').val();
   
   console.log($('#underfloor-heatingInput').val());
   if(someI > 0){
       $('#underfloor-heatingInput0').removeClass('hide');
       $('#underfloor-heatingInput2, #underfloor-heatingInput1').addClass('hide');
   }
   if(someI > 1){
       $('#underfloor-heatingInput1').removeClass('hide');
       $('#underfloor-heatingInput2').addClass('hide');
   }
   if(someI > 2){
       $('#underfloor-heatingInput2').removeClass('hide');
   }
   if(someI == 0){
       $('#underfloor-heatingInput0, #underfloor-heatingInput1, #underfloor-heatingInput2').addClass('hide');
   }
    if(someI == null){
       $('#underfloor-heatingInput0, #underfloor-heatingInput1, #underfloor-heatingInput2').addClass('hide');
   }
});

});

